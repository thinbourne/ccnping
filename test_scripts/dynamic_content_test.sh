#!/bin/bash
# This experiment simulates hosts trying to watch live dynamic 
# content from ccnx:/dynamic/
#    -i defines interval between content requests
#    -c defines total count of chunks requested
#    -p defines the id of the content requested
#       for instance -p channelA -> ccnx:/dynamic/channelA...
#    -n start requesting from chunk # specified...
#
echo "Clearing text files..."
echo '' > client1_dynamic.txt
echo '' > client2_dynamic.txt
echo '' > client3_dynamic.txt
echo "Starting content requests for client1..."
ccnping ccnx:/dynamic -i 0.5 -c 100 -n 1 -p channelA | grep csvstats >> client1_dynamic.txt &
ccnping ccnx:/dynamic -i 0.5 -c 150 -n 1 -p channelB | grep csvstats >> client1_dynamic.txt &
ccnping ccnx:/dynamic -i 0.5 -c 200 -n 1 -p channelC | grep csvstats >> client1_dynamic.txt &
echo "sleep..."
sleep 1
echo "Starting content requests for client2..."
ccnping ccnx:/dynamic -i 0.5 -c 100 -n 1 -p channelA | grep csvstats >> client2_dynamic.txt &
ccnping ccnx:/dynamic -i 0.5 -c 150 -n 1 -p channelB | grep csvstats >> client2_dynamic.txt &
ccnping ccnx:/dynamic -i 0.5 -c 200 -n 1 -p channelC | grep csvstats >> client2_dynamic.txt &
echo "sleep..."
sleep 1
echo "Starting content requests for client3..."
ccnping ccnx:/dynamic -i 0.5 -c 100 -n 1 -p channelA | grep csvstats >> client3_dynamic.txt &
ccnping ccnx:/dynamic -i 0.5 -c 150 -n 1 -p channelB | grep csvstats >> client3_dynamic.txt &
echo "Starting last content request on foreground."
echo "Now just wait, this could take a long time..."
ccnping ccnx:/dynamic -i 0.5 -c 200 -n 1 -p channelC | grep csvstats >> client3_dynamic.txt 
echo "Done!"

