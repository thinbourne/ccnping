N_TESTS=10

echo ""
echo "Running dynamic content request tests"
echo "Test comments:"
echo "$1"
echo "------------------------------"

for ((i=1 ; i <= $N_TESTS ; i++))
do
 echo "Test Run #$i"
 echo "------------------------------"
 ./dynamic_content_test.sh
 echo ""
 echo "csvstats,prefix/id,avg,mdev,pktlost,time"
 cat client?_dynamic.txt
 echo "------------------------------"
done

