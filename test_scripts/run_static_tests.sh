N_TESTS=10

echo ""
echo "Running static content request tests"
echo "Test comments:"
echo "$1"

for ((i=1 ; i <= $N_TESTS ; i++))
do
 echo "Test Run #$i"
 echo "------------------------------"
 ./static_content_test.sh
 echo ""
 echo "csvstats,prefix/id,avg,mdev,pktlost,time"
 cat client?_static.txt
 echo "------------------------------"
done

