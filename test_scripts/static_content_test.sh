#!/bin/bash
# This experiment simulates hosts trying to retrieve static 
# content from ccnx:/static/
#    -a allows to retrieve content cached in routers
#    -i defines interval between content requests
#    -c defines total count of chunks requested
#    -p defines the id of the content requested
#       for instance -p videox -> ccnx:/static/videox...
#    -n start requesting from chunk # specified...
echo "Clearing text files..."
echo '' > client1_static.txt
echo '' > client2_static.txt
echo '' > client3_static.txt
echo "Starting content requests for client1..."
ccnping ccnx:/static -i 0.5 -c 100 -n 1 -p videox -a | grep csvstats >> client1_static.txt &
ccnping ccnx:/static -i 0.5 -c 150 -n 1 -p videoy -a | grep csvstats >> client1_static.txt &
ccnping ccnx:/static -i 0.5 -c 200 -n 1 -p videoz -a | grep csvstats >> client1_static.txt &
echo "sleep..."
sleep 1
echo "Starting content requests for client2..."
ccnping ccnx:/static -i 0.5 -c 100 -n 1 -p videox -a | grep csvstats >> client2_static.txt &
ccnping ccnx:/static -i 0.5 -c 150 -n 1 -p videoy -a | grep csvstats >> client2_static.txt &
ccnping ccnx:/static -i 0.5 -c 200 -n 1 -p videoz -a | grep csvstats >> client2_static.txt &
echo "sleep..."
sleep 1
echo "Starting content requests for client3..."
ccnping ccnx:/static -i 0.5 -c 100 -n 1 -p videox -a | grep csvstats >> client3_static.txt &
ccnping ccnx:/static -i 0.5 -c 150 -n 1 -p videoy -a | grep csvstats >> client3_static.txt &
echo "Starting last content request on foreground."
echo "Now just wait, this could take a long time..."
ccnping ccnx:/static -i 0.5 -c 200 -n 1 -p videoz -a | grep csvstats >> client3_static.txt 
echo "Done!"

